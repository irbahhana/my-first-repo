import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan and Arga Ghulam Ahmad
 *
 * This template is used for Programming Foundations 2 Tutorial 2 Term 2 2017/2018.
 * It is recommended to use this template as the base for completing Tutorial 2.
 * You are allowed to make modifications to the template as long it still matches
 * with the tutorial specification.
 *
 * Code Author (Mahasiswa):
 * @author Muhammad Naufal Irbahhana, NPM 1706019425, Kelas Fprog2, GitLab Account: irbahhana
 */

public class SistemSensus {
    public static void main(String[] args) {
        // Buat input scanner baru
        Scanner input = new Scanner(System.in);

        // TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        // User Interface untuk meminta masukan
        System.out.print("Census Data Printing Program\n" +
                "--------------------\n" +
                "Nama Kepala Keluarga   : ");
        String nama = input.nextLine();
            System.out.print("Address               : ");
        String alamat = input.nextLine();
            System.out.print("Body lenght(cm)       : ");
        int panjang = Integer.parseInt(input.nextLine());
        if ( !(0 < panjang && panjang <= 250) ){
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
            System.out.print("Body width (cm)       : ");
        int lebar = Integer.parseInt(input.nextLine());
            System.out.print("Body Height (cm)      : ");
        int tinggi = Integer.parseInt(input.nextLine());
            System.out.print("Weight (kg)           : ");
        double berat = Double.parseDouble(input.nextLine());
            System.out.print("Family Member:        : ");
        int makanan = Integer.parseInt(input.nextLine());
            System.out.print("Birth date            : ");
        String tanggalLahir = input.nextLine();
            System.out.print("Notes                 : ");
        String catatan = input.nextLine();
            System.out.print("Copies of Data        : ");
        int jumlahCetakan = Integer.parseInt(input.nextLine());

        // TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
        // TODO Hitung rasio berat per volume (rumus lihat soal)
        int rasio = (int)(berat / (panjang * lebar * tinggi*0.000001));
        for (int i=0; i <= jumlahCetakan;i++) {
        // TODO Minta masukan terkait nama penerima hasil cetak data
            System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
            String penerima =input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase

        // TODO Periksa ada catatan atau tidak
            if (catatan.isEmpty()) {
                catatan = "No additional Notes";
            }
			else {
                catatan = catatan;
            }

            // TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
            String hasil = "DATA READY PRINTED FOR " + penerima.toUpperCase() + "\n----------------------\n"
            + nama + " - " + alamat +  "\nBorn on " + tanggalLahir + 
            "\nRatio Weight per Volume : " +rasio+" kg/m^3"+ "\nNotes : " + catatan;

			System.out.println(hasil);
        }
        
        // TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
        // TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)

        int asciivalue = 0;
        for (int i=0; i<nama.length(); i++) {
            char character = nama.charAt(i);
            int charnum = (int)character;

            asciivalue += charnum;
        }

        int nomor = (int) ((panjang * tinggi * lebar) + asciivalue) % 10000;

        // TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
        String nomorKeluarga = (nama.charAt(0) + String.valueOf(nomor));

        // TODO Hitung anggaran makanan per tahun (rumus lihat soal)
        int anggaran = (int) (50000 * 365 * (makanan));

        // TODO Hitung umur dari tanggalLahir (rumus lihat soal)
        String tahunLahir = tanggalLahir.substring(6, 10); // lihat hint jika bingung
        int umur = (int) (2018-Integer.parseInt(tahunLahir));

        // TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
        String Apartment = "";
        String kabupaten = "";

        if (0<=umur && umur<=18) {
            Apartment = "Aryaduta";
            kabupaten = "Beji";
        }
        else if (19<=umur && umur<=1018) {
            if (0<=anggaran && anggaran<=100000000) {
                Apartment = "PV";
                kabupaten = "POCIN";
            }
            else if (100000000<=anggaran) {
                Apartment = "Tamel";
                kabupaten = "Sebelah UI";
            }
        }
        System.out.println(Apartment);
        // TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
        String rekomendasi = "\nREKOMENDASI APARTEMEN\n" +
                "\n--------------------\n" +
                "\nMENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga +
                "\nMENIMBANG: Anggaran makanan tahunan: Rp" + anggaran +
                "\nUmur kepala keluarga: " + umur +" tahun\n" +
                "MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di: \n" +
                Apartment + ", kabupaten " + kabupaten;
        System.out.println(rekomendasi);

        input.close();
        
    }
}
