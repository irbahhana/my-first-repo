<<<<<<< HEAD
Muhammad Naufal Irbahhana

1706019425

KKI Fprog2
=======
# Tutorial & Lab Repository

CSGE601021 Programming Foundations 2 @ Faculty of Computer Science Universitas
Indonesia, Term 2 2017/2018

* * *

This repository contains Tutorial & Lab materials for Programming Foundations 2.

## Table of Contents

1. Lab
    1. [Lab 1](https://gitlab.com/DDP2-CSUI/ddp-lab-ki/blob/master/lab_instructions/lab_1/README.md) - Introduction to Java & Git
    2. [Lab 2](https://gitlab.com/DDP2-CSUI/ddp-lab-ki/blob/master/lab_instructions/lab_2/README.md) - Basic Concepts of Java Programming

* * *

Tools that will be used in this courses are:

- Java Development Kit (JDK) 8
- Git
- Notepad++ (or similar text editor)
- Integrated Development Environment (IDE)
- Gradle
- GitLab Account

Make sure that you install and have the needed tools above. See this [manual][Manual]
for installation and configuration guide.

## Contact Information

Lecturer:

- [Daya Adianto](https://gitlab.com/addianto)
    - Office Hours: Monday/Friday, 4 PM - 5 PM at 3303

TA:

- [Mochamad Aulia Akbar Pratomo](https://gitlab.com/Mochaul)
- [Rizki Maulana Rahmadi](https://gitlab.com/kikirmd)
- LINE@ Dek Depe (in Bahasa): [@nhz2170m][LINE]

<<<<<<< HEAD
- Line Dek Depe : [@nhz2170m](https://line.me/R/ti/p/%40nhz2170m)
>>>>>>> 7b32de54ecb2a566ce1959dc73eaf322d205eb09
=======
[Manual]: https://drive.google.com/file/d/1c1AA-9ju1S82-NYyV7EMyPNwScPpMQsr/view?usp=sharing
[LINE]: https://line.me/R/ti/p/%40nhz2170m)
>>>>>>> 6fa974f3cb2322c89cbf0868159e97f1aad1206e
